# reminder

Things I end up googling a lot

### Push an existing folder

```bash
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:arqser/reminder.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main
```

### Build docker image with golang and air for local development

Create Dockerfile

```dockerfile
./Dockerfile.local
FROM golang:1.22.4

WORKDIR /src

RUN go install github.com/air-verse/air@latest

COPY . .
RUN go mod download

CMD ["air", "-c", ".air.toml"]
```

Build from Dockerfile with `docker build --no-cache -t app:latest -f ./Dockerfile.local .` and run container with `docker run --rm -d --name=app --network=app-network -p 8080:80 -v .:/src app:latest`

### Make dirs recursively

Use `mkdir -p` for parents

### Create symlink

Create new symlink with `ln -s /path/to/file /path/to/symlink`

### Enable query log in MySQL

```mysql
SET GLOBAL log_output = 'TABLE';
SET GLOBAL general_log = 'ON';
```

Convert `BLOB` with `SELECT CONVERT(argument USING utf8) FROM mysql.general_log;`
